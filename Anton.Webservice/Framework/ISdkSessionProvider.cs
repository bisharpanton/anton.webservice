﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ridder.Client.SDK.Extensions;

namespace Anton.Webservice.Framework
{
    public interface ISdkSessionProvider
    {
        SdkSession Get(string company);
        SdkSession GetByCompanyName(string company);
    }
}
