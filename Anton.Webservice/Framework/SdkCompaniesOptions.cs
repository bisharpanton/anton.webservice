﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ridder.AspNetCore.ClientSdk;

namespace Anton.Webservice.Framework
{
    public class SdkCompaniesOptions
    {
        public IDictionary<string, ClientSdkOptions> Companies { get; set; } = new Dictionary<string, ClientSdkOptions>();
    }
}
