﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Ridder.Client.SDK.Extensions;

namespace Anton.Webservice.Framework
{
    public class SdkSessionProvider : ISdkSessionProvider
    {
        private readonly SdkCompaniesOptions _options;

        public SdkSessionProvider(IOptions<SdkCompaniesOptions> options)
        {
            _options = options?.Value ?? throw new ArgumentNullException(nameof(options));
        }

        public SdkSession Get(string company)
        {
            return CreateSdkSession(company);
        }

        public SdkSession GetByCompanyName(string companyName)
        {
            if (!_options.Companies.Any(x => x.Value.CompanyName.Equals(companyName)))
            {
                throw new ConfigurationErrorsException($"Company '{companyName}' not found in settings");
            }

            var companyInfo = _options.Companies.FirstOrDefault(x => x.Value.CompanyName.Equals(companyName));

            return CreateSdkSession(companyInfo.Value);
        }

        protected SdkSession CreateSdkSession(string company)
        {
            if (!_options.Companies.TryGetValue(company, out var sdkOptions))
            {
                throw new ConfigurationErrorsException($"Company '{company}' not found in settings");
            }

            var config = new SdkConfiguration() {
                CompanyName = sdkOptions.CompanyName,
                UserName = sdkOptions.UserName,
                Password = sdkOptions.Password,
                Permission = sdkOptions.Permission,
                RidderIQClientPath = sdkOptions.RidderIQClientPath
            };

            return new SdkSession(config);
        }

        protected SdkSession CreateSdkSession(Ridder.AspNetCore.ClientSdk.ClientSdkOptions sdkOptions)
        {
            var config = new SdkConfiguration()
            {
                CompanyName = sdkOptions.CompanyName,
                UserName = sdkOptions.UserName,
                Password = sdkOptions.Password,
                Permission = sdkOptions.Permission,
                RidderIQClientPath = sdkOptions.RidderIQClientPath
            };

            return new SdkSession(config);
        }
    }
}
