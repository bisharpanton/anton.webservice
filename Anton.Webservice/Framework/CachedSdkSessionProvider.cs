﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Ridder.Client.SDK.Extensions;

namespace Anton.Webservice.Framework
{
    public class CachedSdkSessionProvider : ISdkSessionProvider
    {
        private readonly ConcurrentDictionary<string, SdkSession> _sessions = new ConcurrentDictionary<string, SdkSession>();
        private readonly SdkCompaniesOptions _options;

        public CachedSdkSessionProvider(IOptions<SdkCompaniesOptions> options)
        {
            _options = options?.Value ?? throw new ArgumentNullException(nameof(options));
        }

        public SdkSession Get(string company)
        {
            var sdk = _sessions.GetOrAdd(company, CreateSdkSession);
            return sdk;
        }

        public SdkSession GetByCompanyName(string companyName)
        {
            if(_sessions.Any(x => x.Value.Configuration.CompanyName.Equals(companyName)))
            {
                return _sessions.First(x => x.Value.Configuration.CompanyName.Equals(companyName)).Value;
            }

            if (!_options.Companies.Any(x => x.Value.CompanyName.Equals(companyName)))
            {
                return null;
            }

            var companyInfo = _options.Companies.FirstOrDefault(x => x.Value.CompanyName.Equals(companyName));

            return CreateSdkSession(companyInfo.Value);
        }

        protected SdkSession CreateSdkSession(string company)
        {
            if (!_options.Companies.TryGetValue(company, out var sdkOptions))
            {
                return null; //Company '{company}' not found in settings
                //throw new ConfigurationErrorsException($"Company '{company}' not found in settings");
            }

            var config = new SdkConfiguration() {
                CompanyName = sdkOptions.CompanyName,
                UserName = sdkOptions.UserName,
                Password = sdkOptions.Password,
                Permission = sdkOptions.Permission,
                RidderIQClientPath = sdkOptions.RidderIQClientPath
            };

            return new SdkSession(config);
        }

        protected SdkSession CreateSdkSession(Ridder.AspNetCore.ClientSdk.ClientSdkOptions sdkOptions)
        {
            var config = new SdkConfiguration()
            {
                CompanyName = sdkOptions.CompanyName,
                UserName = sdkOptions.UserName,
                Password = sdkOptions.Password,
                Permission = sdkOptions.Permission,
                RidderIQClientPath = sdkOptions.RidderIQClientPath
            };

            return new SdkSession(config);
        }
    }
}
