﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Anton.Webservice.Models
{
    public class SafetyReport
    {
        public string CompanyName { get; set; }
        
        public string Description { get; set; }
        public string Cause { get; set; }
        public string Measures { get; set; }
        public string LongTermMeasures { get; set; }

        public string Source { get; set; }
        public string Kind { get; set; }
        public string Type { get; set; }
        public string InterCompanyRelation { get; set; }
    }
}
