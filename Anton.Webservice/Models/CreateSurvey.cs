﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Anton.Webservice.Models
{
    public class CreateSurvey
    {
        public string IntranetSurveyId { get; set; }
        public List<CreateSurveyDetail> CreateSurveyDetails { get; set; }
    }
}
