﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Anton.Webservice.Models
{
    public class CreateSurveyDetail
    {
        public Category Category { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
    }

    public enum Category : int
    {
        Quality = 1,
        Safety = 2,
        Environment = 3,
        Financial = 4,
        Finally = 5,
        General = 6
    }
}
