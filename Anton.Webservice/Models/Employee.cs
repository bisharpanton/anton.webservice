﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Anton.Webservice.Models
{
    public class Employee
    {
        public string Company { get; set; }
        public string AbbreviationCompany { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Position { get; set; }
        public DateTime? EmploymentStartDate { get; set; }
        public string PhoneNumber { get; set; }
        public string ShortPhoneNumber { get; set; }
        public string Email { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public int EmployeeId { get; set; }
        public string UniqueEmployeeIndex { get; set; }
        public DateTime? DatePictureChanged { get; set; }
    }
}
