﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Anton.Webservice.Models
{
    public class Result
    {
        public bool HasError { get; set; }
        public string Message { get; set; }
    }
}
