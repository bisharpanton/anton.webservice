﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Anton.Webservice.Models
{
    public class Survey
    {
        public Guid AllSurveyId { get; set; }
        public string AbbrevationCompany { get; set; }
        public string Company { get; set; }
        public string IntranetSurveyId { get; set; }
        public int CompanySurveyId { get; set; }
        public string RelationName { get; set; }
    }
}
