﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Anton.Webservice.Framework;
using Anton.Webservice.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace Anton.Webservice.Controllers
{
    [Route("api")]
    [ApiController]
    public class CompaniesController : ControllerBase
    {
        private readonly SdkCompaniesOptions _options;
        private readonly ISdkSessionProvider _sessionProvider;

        public CompaniesController(IOptions<SdkCompaniesOptions> options, ISdkSessionProvider sessionProvider)
        {
            _options = options?.Value ?? throw new ArgumentNullException(nameof(options));
            _sessionProvider = sessionProvider;
        }

        [HttpGet("[controller]")]
        public ActionResult<List<string>> Get()
        {
            return _options.Companies.Select(x => x.Key).ToList();
        }

        [HttpGet("CompanyAddressInfo")]
        public ActionResult<List<CompanyAddressInfo>> GetCompanyAddressInfo()
        {
            var sdk = _sessionProvider.Get("AIO");

            var companiesAddressInfo = sdk.GetRecordsetColumns("U_ALLCOMPANYADDRESSINFO", "", "")
                .DataTable.AsEnumerable().Select(x => new CompanyAddressInfo()
                {
                    Company = x.Field<string>("COMPANY"),
                    AbbreviationCompany = x.Field<string>("ABBREVIATIONCOMPANY"),
                    Street = x.Field<string>("STREET"),
                    HouseNumber = x.Field<string>("HOUSENUMBER"),
                    ZipCode = x.Field<string>("ZIPCODE"),
                    City = x.Field<string>("CITY"),
                    Country = x.Field<string>("COUNTRY"),
                }).ToList();

            return Ok(companiesAddressInfo);
        }
    }
}
