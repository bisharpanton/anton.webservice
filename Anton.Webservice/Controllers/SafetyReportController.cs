﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Anton.Webservice.Framework;
using Anton.Webservice.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Ridder.Client.SDK.Extensions;

namespace Anton.Webservice.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class SafetyReportController : ControllerBase
    {
        private readonly ISdkSessionProvider _sessionProvider;

        public SafetyReportController(ISdkSessionProvider sessionProvider)
        {
            _sessionProvider = sessionProvider;
        }

        // POST: api/Surveys
        [HttpPost]
        public ActionResult<SafetyReport> Post([FromBody] SafetyReport safetyReport)
        {
            var sdk = _sessionProvider.GetByCompanyName(safetyReport.CompanyName);

            if (sdk == null)
            {
                return NotFound($"Company '{safetyReport.CompanyName}' not found.");
            }

            var choiceIdType = 0;
            var choiceIdIntercompanyRelation = 0;

            var validateMessage = ValidateSafetyReport(sdk, safetyReport, ref choiceIdType, ref choiceIdIntercompanyRelation);

            if(!string.IsNullOrEmpty(validateMessage))
            {
                return BadRequest(validateMessage);
            }

            var errorMessage = string.Empty;
            var createdSafetyReportId = CreateSafetyReportCore(sdk, safetyReport, choiceIdType, choiceIdIntercompanyRelation, ref errorMessage);

            if(!string.IsNullOrEmpty(errorMessage))
            {
                return StatusCode(500, errorMessage);
            }

            return Ok(createdSafetyReportId);
        }


        // POST: api/UploadFile
        [HttpPost("UploadFile")]
        //public ActionResult<SafetyReport> UploadFile(string companyName, int safetyReportId, string fileName, string file, string extension)
        public ActionResult<SafetyReport> UploadFile([FromBody] UploadFileRequest request)
        {
            var sdk = _sessionProvider.GetByCompanyName(request.CompanyName);

            if (sdk == null)
            {
                return NotFound($"Company '{request.CompanyName}' not found.");
            }

            var validateResult = ValidateParameters(sdk, request.SafetyReportId, request.File);

            if (!string.IsNullOrEmpty(validateResult))
            {
                return BadRequest(validateResult);
            }

            var errorMessage = string.Empty;

            //Veiligheidsmeldingen worden gedistribueerd over meerdere bedrijven. Daarom slaan we deze op in de database.

            var createdFileName = CreateFileOnArchiveLocation(sdk, request.FileName, request.File, request.Extension, request.SafetyReportId, ref errorMessage);

            if(!string.IsNullOrEmpty(errorMessage))
            {
                return StatusCode(500, errorMessage);
            }

            ProcessDocumentToRidderiQ(sdk, request.SafetyReportId, createdFileName, ref errorMessage);

            if (!string.IsNullOrEmpty(errorMessage))
            {
                return StatusCode(500, errorMessage);
            }

            //Omdat ze in de database opgeslagen worden, verwijder ze weer
            System.IO.File.Delete(createdFileName);

            return Ok("Document correct toegevoegd in Ridder iQ");
        }

        private void ProcessDocumentToRidderiQ(SdkSession sdk, int recordId, string createdFileName, ref string errorMessage)
        {
            //Veiligheidsmeldingen worden gedistribueerd over meerdere bedrijven. Daarom slaan we deze op in de database.

            var result = sdk.Sdk.EventsAndActions.CRM.Actions.AddDocument(createdFileName, "0", false,
                "C_VEILIGHEIDSMELDINGEN", recordId, Path.GetFileNameWithoutExtension(createdFileName));

            if (result.HasError)
            {
                errorMessage = $"Toevoegen document in Ridder iQ is mislukt, oorzaak: {result.GetResult()}";
            }
        }

        private string ValidateParameters(SdkSession sdk, int safetyReportId, string file)
        {
            if (file == null)
            {
                return "No file found.";
            }

            var recordExists = sdk.GetRecordsetColumns("C_VEILIGHEIDSMELDINGEN", "PK_C_VEILIGHEIDSMELDINGEN", $"PK_C_VEILIGHEIDSMELDINGEN = {safetyReportId}").RecordCount > 0;

            if (!recordExists)
            {
                return $"Veiligheidsmelding met id {safetyReportId} niet gevonden.";
            }

            return string.Empty;
        }

        private string CreateFileOnArchiveLocation(SdkSession sdk, string fileName, string file, string extension, int safetyReportId, ref string errorMessage)
        {
            var targetDirectory = DetermineTargetDirectory(sdk, safetyReportId, ref errorMessage);

            if (!string.IsNullOrEmpty(errorMessage))
            {
                return string.Empty;
            }

            if (!Directory.Exists(targetDirectory))
            {
                Directory.CreateDirectory(targetDirectory);
            }

            var createdFileName = Path.Combine(targetDirectory, $"{fileName}.{extension.TrimStart('.')}");

            try
            {
                /*
                using (var stream = new FileStream(createdFileName, FileMode.Create))
                {
                    file.CopyTo(stream);
                }*/

                System.IO.File.WriteAllBytes(createdFileName, Convert.FromBase64String(file));
            }
            catch (Exception e)
            {
                errorMessage = $"Archiveren file is mislukt, oorzaak: {e}";
                return string.Empty;
            }

            return createdFileName;
        }

        private string DetermineTargetDirectory(SdkSession sdk, int safetyReportId, ref string errorMessage)
        {
            var generalLocationAppreoAppDocuments = sdk.GetRecordsetColumns("R_CRMSETTINGS", "LOCATIONAPPREOAPPDOCUMENTS", "")
                .DataTable.AsEnumerable().First().Field<string>("LOCATIONAPPREOAPPDOCUMENTS");

            if (string.IsNullOrEmpty(generalLocationAppreoAppDocuments))
            {
                errorMessage = $"No location Appreo app documents found in the Ridder CRM-settings.";
                return string.Empty;
            }

            return Path.Combine(generalLocationAppreoAppDocuments, "Veiligheidsmeldingen", safetyReportId.ToString());
        }

        private int CreateSafetyReportCore(SdkSession sdk, SafetyReport safetyReport, int choiceIdType, int choiceIdIntercompanyRelation, ref string errorMessage)
        {
            var safetyReportSourceId = GetOrCreateSafetyReportSource(sdk, safetyReport.Source, ref errorMessage);

            if (safetyReportSourceId == 0)
            {
                return 0;
            }

            var safetyReportKindId = GetOrCreateSafetyReportKind(sdk, safetyReport.Kind, ref errorMessage);

            if (safetyReportKindId == 0)
            {
                return 0;
            }


            var rsSafetyReport = sdk.GetRecordsetColumns("C_VEILIGHEIDSMELDINGEN", "", "PK_C_VEILIGHEIDSMELDINGEN IS NULL");
            rsSafetyReport.UseDataChanges = true;
            rsSafetyReport.UpdateWhenMoveRecord = false;

            rsSafetyReport.AddNew();
            rsSafetyReport.SetFieldValue("DESCRIPTION", safetyReport.Description);
            rsSafetyReport.SetFieldValue("CAUSE", safetyReport.Cause);
            rsSafetyReport.SetFieldValue("MEASURES", safetyReport.Measures);
            rsSafetyReport.SetFieldValue("LONGTERMMEASURES", safetyReport.LongTermMeasures);

            rsSafetyReport.SetFieldValue("FK_BRONMELDINGEN", safetyReportSourceId);
            rsSafetyReport.SetFieldValue("FK_MELDINGSOORTEN", safetyReportKindId);
            
            rsSafetyReport.SetFieldValue("INCIDENTREPORTING", choiceIdType);

            if(choiceIdIntercompanyRelation != 0)
            {
                rsSafetyReport.SetFieldValue("INTERCOMPANYRELATION", choiceIdIntercompanyRelation);
            }

            var updateResult = rsSafetyReport.Update2();

            if(updateResult.Any(x => x.HasError))
            {
                errorMessage = $"Opslaan veiligheidsmelding is mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}";
                return 0;
            }

            return (int)updateResult.First().PrimaryKey;
        }

        private int GetOrCreateSafetyReportKind(SdkSession sdk, string kind, ref string errorMessage)
        {
            if (string.IsNullOrEmpty(kind))
            {
                errorMessage = "Geen soort melding gevonden.";
                return 0;
            }

            var rsMeldingssoorten = sdk.GetRecordsetColumns("C_MELDINGSOORTEN", "DESCRIPTION", $"DESCRIPTION = '{kind}'");

            if (rsMeldingssoorten.RecordCount > 0)
            {
                return rsMeldingssoorten.DataTable.AsEnumerable().First().Field<int>("PK_C_MELDINGSOORTEN");
            }

            rsMeldingssoorten.AddNew();
            rsMeldingssoorten.SetFieldValue("DESCRIPTION", kind);
            var updateResult = rsMeldingssoorten.Update2();

            if (updateResult.Any(x => x.HasError))
            {
                errorMessage = $"Aanmaken nieuwe soort is mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}";
                return 0;
            }

            return (int)updateResult.First().PrimaryKey;
        }

        private int GetOrCreateSafetyReportSource(SdkSession sdk, string source, ref string errorMessage)
        {
            if (string.IsNullOrEmpty(source))
            {
                errorMessage = "Geen bron gevonden.";
                return 0;
            }

            var rsBronmelding = sdk.GetRecordsetColumns("C_BRONMELDINGEN", "DESCRIPTION", $"DESCRIPTION = '{source}'");
            
            if(rsBronmelding.RecordCount > 0)
            {
                return rsBronmelding.DataTable.AsEnumerable().First().Field<int>("PK_C_BRONMELDINGEN");
            }

            rsBronmelding.AddNew();
            rsBronmelding.SetFieldValue("DESCRIPTION", source);
            var updateResult = rsBronmelding.Update2();

            if (updateResult.Any(x => x.HasError))
            {
                errorMessage = $"Aanmaken nieuwe bron is mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}";
                return 0;
            }

            return (int)updateResult.First().PrimaryKey;
        }

        private string ValidateSafetyReport(SdkSession sdk, SafetyReport safetyReport, ref int choiceIdType, ref int choiceIdIntercompanyRelation)
        {
            if (string.IsNullOrEmpty(safetyReport.Description))
            {
                return "Omschrijving is verplicht.";
            }

            if (string.IsNullOrEmpty(safetyReport.Source))
            {
                return "Bron melding is verplicht.";
            }

            if (string.IsNullOrEmpty(safetyReport.Kind))
            {
                return "Soort melding is verplicht.";
            }

            if (string.IsNullOrEmpty(safetyReport.Type))
            {
                return "Incident melding is verplicht.";
            }

            var safetyReportTypes = sdk.GetRecordsetColumns("M_CHOICEVALUE", "CHOICENUMBER, CHOICETEXT", "FK_CHOICETYPE = 'e7104994-fc6c-4dfc-bb14-d703dd006a8f'")
                .DataTable.AsEnumerable().Select(x => new ChoiceValue(x.Field<int>("CHOICENUMBER"), x.Field<string>("CHOICETEXT"))).ToList();

            if(!safetyReportTypes.Select(x => x.Description).ToList().Contains(safetyReport.Type, StringComparer.OrdinalIgnoreCase))
            {
                return $"Type '{safetyReport.Type}' is geen geldig type.";
            }

            choiceIdType = safetyReportTypes.First(x => x.Description.Equals(safetyReport.Type, StringComparison.OrdinalIgnoreCase)).ChoiceId;

            if(!string.IsNullOrEmpty(safetyReport.InterCompanyRelation))
            {
                var intercompanyRelations = sdk.GetRecordsetColumns("M_CHOICEVALUE", "CHOICENUMBER, CHOICETEXT", "FK_CHOICETYPE = '06f64738-17bb-4d09-814a-c458ed132b22'")
                    .DataTable.AsEnumerable().Select(x => new ChoiceValue(x.Field<int>("CHOICENUMBER"), x.Field<string>("CHOICETEXT"))).ToList();

                if (!intercompanyRelations.Select(x => x.Description).ToList().Contains(safetyReport.InterCompanyRelation, StringComparer.OrdinalIgnoreCase))
                {
                    return $"Type '{safetyReport.InterCompanyRelation}' is geen geldige intercompany relatie.";
                }

                choiceIdIntercompanyRelation = intercompanyRelations.First(x => x.Description.Equals(safetyReport.InterCompanyRelation, StringComparison.OrdinalIgnoreCase)).ChoiceId;
            }

            return string.Empty;
        }



        private class ChoiceValue
        {
            public ChoiceValue(int cId, string d)
            {
                ChoiceId = cId;
                Description = d;
            }

            public int ChoiceId { get; set; }
            public string Description { get; set; }
        }

        public class UploadFileRequest
        {
            public string CompanyName { get; set; }
            public int SafetyReportId { get; set; }
            public string FileName { get; set; }
            public string File { get; set; }
            public string Extension { get; set; }
        }
    }
}
