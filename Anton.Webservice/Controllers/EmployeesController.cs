﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Anton.Webservice.Framework;
using Anton.Webservice.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Ridder.Client.SDK.Extensions;

namespace Anton.Webservice.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class EmployeesController : Controller
    {
        private readonly ISdkSessionProvider _sessionProvider;

        public EmployeesController(ISdkSessionProvider sessionProvider)
        {
            _sessionProvider = sessionProvider;
        }

        // GET api/employees
        [HttpGet]
        public List<Employee> Get()
        {
            var sdk = _sessionProvider.Get("AIO");
            
            return sdk.GetRecordsetColumns("C_ALLEMPLOYEES", "", "")
                .DataTable.AsEnumerable().Select(x => new Employee()
                {
                    Company = x.Field<string>("COMPANY"),
                    AbbreviationCompany = x.Field<string>("ABBREVIATIONCOMPANY"),
                    FirstName = x.Field<string>("FIRSTNAME"),
                    LastName = x.Field<string>("LASTNAME"),
                    Position = x.Field<string>("POSITION"),
                    EmploymentStartDate = x.Field<DateTime?>("EMPLOYMENTSTARTDATE"),
                    PhoneNumber = x.Field<string>("PHONENUMBER"),
                    ShortPhoneNumber = x.Field<string>("SHORTPHONENUMBER"),
                    Email = x.Field<string>("EMAIL"),
                    DateOfBirth = x.Field<DateTime?>("DATEOFBIRTH"),
                    EmployeeId = x.Field<int>("EMPLOYEEID"),
                    UniqueEmployeeIndex = x.Field<string>("UNIQUEEMPLOYEEINDEX"),
                    DatePictureChanged = x.Field<DateTime?>("DATEPICTURECHANGED"),
                }).ToList();
        }

        // GET: api/Employees/ARI_5
        [HttpGet("{id}", Name = "Get")]
        public ActionResult<Employee> Get(string id)
        {
            var sdk = _sessionProvider.Get("AIO");

            var rsEmployee = sdk.GetRecordsetColumns("C_ALLEMPLOYEES", "",
                $"UNIQUEEMPLOYEEINDEX = '{id}'");

            if (rsEmployee.RecordCount == 0)
            {
                return NotFound();
            }

            var employee = rsEmployee.DataTable.AsEnumerable().Select(x => new Employee()
            {
                Company = x.Field<string>("COMPANY"),
                AbbreviationCompany = x.Field<string>("ABBREVIATIONCOMPANY"),
                FirstName = x.Field<string>("FIRSTNAME"),
                LastName = x.Field<string>("LASTNAME"),
                Position = x.Field<string>("POSITION"),
                EmploymentStartDate = x.Field<DateTime?>("EMPLOYMENTSTARTDATE"),
                PhoneNumber = x.Field<string>("PHONENUMBER"),
                ShortPhoneNumber = x.Field<string>("SHORTPHONENUMBER"),
                Email = x.Field<string>("EMAIL"),
                DateOfBirth = x.Field<DateTime?>("DATEOFBIRTH"),
                EmployeeId = x.Field<int>("EMPLOYEEID"),
                UniqueEmployeeIndex = x.Field<string>("UNIQUEEMPLOYEEINDEX"),
                DatePictureChanged = x.Field<DateTime?>("DATEPICTURECHANGED"),
            }).First();

            return employee;
        }

        // GET: api/Employees/GetPicture/{id}
        [HttpGet("GetPicture/{id}")]
        public ActionResult<string> GetPicture(string id)
        {
            var sdk = _sessionProvider.Get("AIO");

            var rsEmployee = sdk.GetRecordsetColumns("C_ALLEMPLOYEES", "PICTURE",
                $"UNIQUEEMPLOYEEINDEX = '{id}'");

            if (rsEmployee.RecordCount == 0)
            {
                return NotFound();
            }

            var picture = rsEmployee.DataTable.AsEnumerable().First().Field<byte[]>("PICTURE");

            if (picture == null)
            {
                return NotFound();
            }

            return Convert.ToBase64String(picture);
        }
    }
}
