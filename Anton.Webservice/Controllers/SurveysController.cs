﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Anton.Webservice.Framework;
using Anton.Webservice.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Ridder.Client.SDK.Extensions;

namespace Anton.Webservice.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SurveysController : ControllerBase
    {
        private readonly ISdkSessionProvider _sessionProvider;

        public SurveysController(ISdkSessionProvider sessionProvider)
        {
            _sessionProvider = sessionProvider;
        }

        // GET: api/Surveys
        [HttpGet]
        public ActionResult<List<Survey>> Get()
        {
            return GetAllSurveys();
        }

        // POST: api/Surveys
        [HttpPost]
        public ActionResult<Survey> Post([FromBody] CreateSurvey createSurvey)
        {
            if (string.IsNullOrEmpty(createSurvey.IntranetSurveyId))
            {
                return BadRequest("Intranet survey id is required.");
            }

            var allSurveys = GetAllSurveys();

            var survey =
                allSurveys.FirstOrDefault(x => x.IntranetSurveyId.Equals(createSurvey.IntranetSurveyId));

            if (survey == null)
            {
                return BadRequest($"No existing survey found with intranet survey id {createSurvey.IntranetSurveyId}.");
            }

            var sdk = _sessionProvider.Get(survey.AbbrevationCompany);

            if (sdk == null)
            {
                return NotFound($"Company '{survey.AbbrevationCompany}' not found.");
            }

            var result = CreateSurveyDetailsInRidderIq(sdk, survey, createSurvey.CreateSurveyDetails);

            if (result.HasError)
            {
                return BadRequest(result.Message);
            }

            return Ok("Survey details correct created in Ridder iQ.");
        }

        private List<Survey> GetAllSurveys()
        {
            var sdk = _sessionProvider.Get("AIO");
            
            return sdk.GetRecordsetColumns("C_ALLSURVEYS",
                    "PK_C_ALLSURVEYS, ABBREVIATIONCOMPANY, COMPANY, RELATIONNAME, KLANTTEVREDENHEIDSONDERZOEKID, INTRANETSURVEYID",
                    "")
                .DataTable.AsEnumerable().Select(x => new Survey()
                {
                    AllSurveyId = (Guid)x["PK_C_ALLSURVEYS"],
                    AbbrevationCompany = (string)x["ABBREVIATIONCOMPANY"],
                    Company = (string)x["COMPANY"],
                    CompanySurveyId = (int)x["KLANTTEVREDENHEIDSONDERZOEKID"],
                    RelationName = (string)x["RELATIONNAME"],
                    IntranetSurveyId = (string)x["INTRANETSURVEYID"],
                }).ToList();
        }

        private Result CreateSurveyDetailsInRidderIq(SdkSession sdk, Survey survey,
            List<CreateSurveyDetail> createSurveyDetails)
        {
            if (!createSurveyDetails.Any())
            {
                return new Result() {HasError = false, Message = ""};
            }
                    
            var rsSurveyQuestions = sdk.GetRecordsetColumns("C_KLANTTEVREDENHEIDSONDERZOEKVRAGEN",
                "", "PK_C_KLANTTEVREDENHEIDSONDERZOEKVRAGEN = -1");
            
            rsSurveyQuestions.UpdateWhenMoveRecord = false;
            rsSurveyQuestions.UseDataChanges = true;

            foreach (CreateSurveyDetail createSurveyDetail in createSurveyDetails)
            {
                rsSurveyQuestions.AddNew();
                rsSurveyQuestions.SetFieldValue("FK_KLANTTEVREDENHEIDSONDERZOEK", survey.CompanySurveyId);
                rsSurveyQuestions.SetFieldValue("CATEGORY", (int)createSurveyDetail.Category);
                rsSurveyQuestions.SetFieldValue("QUESTION", createSurveyDetail.Question);
                rsSurveyQuestions.SetFieldValue("ANSWERINTEXT", createSurveyDetail.Answer);
            }

            var updateResultSurveyQuestions = rsSurveyQuestions.Update2();

            if (updateResultSurveyQuestions.Any(x => x.HasError))
            {
                return new Result()
                {
                    HasError = true,
                    Message = $"Creating survey questions failed, cause: {updateResultSurveyQuestions.First(x => x.HasError).GetResult()}",
                };
            }

            //Update survey
            var rsSurvey = sdk.GetRecordsetColumns("C_KLANTTEVREDENHEIDSONDERZOEK",
                "IMPORTEDFROMINTRANET", 
                $"PK_C_KLANTTEVREDENHEIDSONDERZOEK = {survey.CompanySurveyId}");
            rsSurvey.MoveFirst();
            rsSurvey.SetFieldValue("IMPORTEDFROMINTRANET", true);
            
            var updateResultSurvey = rsSurvey.Update2();

            if (updateResultSurvey.Any(x => x.HasError))
            {
                return new Result()
                {
                    HasError = true,
                    Message = $"Updating survey failed, cause: {updateResultSurvey.First(x => x.HasError).GetResult()}",
                };
            }

            return new Result() {HasError = false, Message = ""};
        }
    }
}
